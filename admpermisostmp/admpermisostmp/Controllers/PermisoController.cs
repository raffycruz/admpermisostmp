﻿using Microsoft.AspNetCore.Mvc;
using Project.Contract;
using Project.SqlRepository.CustomModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace admpermisostmp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PermisoController : ControllerBase
    {
        private readonly IPermisoService<Permiso> permisoService;

        public PermisoController(IPermisoService<Permiso> PermisoService)
        {
            this.permisoService = PermisoService;
        }

        // GET api/permiso
        [HttpGet]
        public async Task<IEnumerable<Permiso>> Get()
        {
            var models = await permisoService.Get();
            return models;
        }

        // GET api/permiso/2
        [HttpGet("{id}")]
        public async Task<ActionResult<Permiso>> Get(int id)
        {
            var permisos = await permisoService.GetById(id);

            if (permisos == null) return NotFound();
            return permisos;
        }

        // POST api/Permiso
        [HttpPost]
        public async Task<ActionResult<Permiso>> Post([FromBody] Permiso Permiso)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            await permisoService.Insert(Permiso);
            return CreatedAtAction(nameof(Get), new { Permiso.Id }, Permiso);
        }

        // PUT api/Permiso/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int Id, [FromBody] Permiso Permiso)
        {
            if (Permiso.Id != Id) return BadRequest();
            if (!ModelState.IsValid) return BadRequest(ModelState);

            await permisoService.Update(Permiso);

            return NoContent();

        }

        // DELETE api/Permiso/2
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var permiso = await permisoService.GetById(id);

            if (permiso == null) return NotFound();

            await permisoService.Delete(permiso);

            return Ok(new { message = "Registro eliminado con éxito!"});
        }
    }
}
