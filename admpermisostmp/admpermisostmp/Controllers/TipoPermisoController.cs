﻿using Microsoft.AspNetCore.Mvc;
using Project.Contract;
using Project.SqlRepository.CustomModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace admpermisostmp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TipoPermisoController : ControllerBase
    {
        private readonly ITipoPermisoService<TipoPermiso> TipoPermisoService;

        public TipoPermisoController(ITipoPermisoService<TipoPermiso> TipoPermisoService)
        {
            this.TipoPermisoService = TipoPermisoService;
        }

        // GET api/TipoPermiso
        [HttpGet]
        public async Task<IEnumerable<TipoPermiso>> Get()
        {
            var models = await TipoPermisoService.Get();
            return models;
        }

        // GET api/TipoPermiso/2
        [HttpGet("{id}")]
        public async Task<ActionResult<TipoPermiso>> Get(int id)
        {
            var TipoPermisos = await TipoPermisoService.GetById(id);

            if (TipoPermisos == null) return NotFound();
            return TipoPermisos;
        }

        // POST api/TipoPermiso
        [HttpPost]
        public async Task<ActionResult<TipoPermiso>> Post([FromBody] TipoPermiso TipoPermiso)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            await TipoPermisoService.Insert(TipoPermiso);
            return CreatedAtAction(nameof(Get), new { TipoPermiso.Id }, TipoPermiso);
        }

        // PUT api/TipoPermiso/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int Id, [FromBody] TipoPermiso TipoPermiso)
        {
            if (TipoPermiso.Id != Id) return BadRequest();
            if (!ModelState.IsValid) return BadRequest(ModelState);

            await TipoPermisoService.Update(TipoPermiso);

            return NoContent();

        }

        // DELETE api/TipoPermiso/2
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var TipoPermiso = await TipoPermisoService.GetById(id);

            if (TipoPermiso == null) return NotFound();

            await TipoPermisoService.Delete(TipoPermiso);

            return NoContent();
        }
    }
}
