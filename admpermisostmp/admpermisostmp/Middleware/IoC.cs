﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Project.Contract;
using Project.SqlRepository;
using Project.SqlRepository.Services;
using Project.SqlRepository.CustomModel;

namespace admpermisostmp.Middleware
{
    public static class IoC
    {
        public static IServiceCollection AddDependency(this IServiceCollection services)
        {
            services.AddScoped<IDB_admPermisostemp_Context, DB_admPermisostemp_Context>();
            services.AddScoped<IPermisoService<Permiso>, PermisoService>();
            services.AddScoped<ITipoPermisoService<TipoPermiso>, TipoPermisoService>();

            services.AddScoped<IUnitOfWork, UnitOfWork>();

            return services;
        }
    }
}
