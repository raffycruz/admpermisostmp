﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Project.Contract
{
    public interface IUnitOfWork
    {
        IBaseRepository<T> GetRepository<T>() where T : class;
        IDatabaseTransaction BeginTransaction();
        void Save();
        Task<int> Save_async();
        EntityEntry Entry(object entity);
    }
}
