﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project.Contract
{
    public interface IDatabaseTransaction
    {
        void Commit();
        void Rollback();
    }
}
