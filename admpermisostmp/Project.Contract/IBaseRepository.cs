﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Project.Contract
{
    public interface IBaseRepository<T> where T : class
    {
        IEnumerable<T> Get();
        IEnumerable<T> Get(Expression<Func<T, bool>> filter);
        IEnumerable<T> Get(Func<IQueryable<T>, IOrderedQueryable<T>> orderBy);
        IEnumerable<T> Get(Expression<Func<T, bool>> filter, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy);
        IEnumerable<T> Get(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includeProperties = "", bool NoTrancking = false);
        T GetById(object id);
        T GetById(params object[] id);
        EntityEntry<T> Insert(T entity);
        void AddRange(IEnumerable<T> entities);
        void RemoveRange(IEnumerable<T> entities);
        void Delete(object id);
        void Delete(params object[] id);
        void Delete(T entityToDelete);
        void Update(T entityToUpdate);
        bool Exists();
        bool Exists(Expression<Func<T, bool>> expr);

        Task<IEnumerable<T>> Get_async();
        Task<IEnumerable<T>> Get_async(Expression<Func<T, bool>> filter);
        Task<IEnumerable<T>> Get_async(Func<IQueryable<T>, IOrderedQueryable<T>> orderBy);
        Task<IEnumerable<T>> Get_async(Func<IQueryable<T>, IIncludableQueryable<T, object>> include);
        Task<IEnumerable<T>> Get_async(Expression<Func<T, bool>> filter, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy);
        Task<IEnumerable<T>> Get_async(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null, bool NoTrancking = false);
        Task<T> GetById_async(object id);
        Task<T> GetById_async(params object[] id);
        Task<IEnumerable<T>> Get_async(Expression<Func<T, bool>> filter, Func<IQueryable<T>, IIncludableQueryable<T, object>> include);
        Task<int> Count_async();
    }
}
