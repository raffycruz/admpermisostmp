﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Project.Contract
{
    public interface IService<T> where T : class
    {
        Task<IEnumerable<T>> Get();
        Task<T> GetById(int id);
        Task<T> Insert(T entityToInsert);
        Task<T> Update(T entityToEdit);
        Task<int> Delete(object id);
        Task<int> Delete(params object[] id);
        Task<int> Delete(T entityToDelete);
        Task<int> Count();
    }
}
