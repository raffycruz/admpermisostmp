﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Query;
using Project.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Project.SqlRepository
{
    public class BaseRepository<T> : IBaseRepository<T> where T : class
    {
        internal IDB_Context db_Context;
        internal DbSet<T> dbSet;

        public BaseRepository(IDB_Context db_Context)
        {
            this.db_Context = db_Context;
            dbSet = db_Context.Set<T>();
        }

        public virtual IEnumerable<T> Get()
        {
            return Get(null, null, "");
        }

        public virtual IEnumerable<T> Get(Expression<Func<T, bool>> filter)
        {
            return Get(filter, null, "");
        }

        public virtual IEnumerable<T> Get(Func<IQueryable<T>, IOrderedQueryable<T>> orderBy)
        {
            return Get(null, orderBy, "");
        }

        public virtual IEnumerable<T> Get(Expression<Func<T, bool>> filter, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy)
        {
            return Get(filter, orderBy, "");
        }

        public virtual IEnumerable<T> Get(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includeProperties = "", bool NoTrancking = false)
        {
            IQueryable<T> query;

            if (NoTrancking == true)
            {
                query = dbSet.AsNoTracking();
            }
            else
            {
                query = dbSet;
            }

            if (filter != null) query = query.Where(filter);

            foreach (var includeProperty in includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null) return orderBy(query).ToList(); else return query.ToList();
        }

        public virtual T GetById(object id)
        {
            return dbSet.Find(id);
        }

        public virtual T GetById(params object[] id)
        {
            return dbSet.Find(id);
        }

        public virtual EntityEntry<T> Insert(T entity)
        {
            return dbSet.Add(entity);
        }

        //public virtual void AddOrUpdate(IEnumerable<T> entities)
        //{
        //    dbSet.AddOrUpdate(entities.ToArray());
        //}

        //public virtual void AddOrUpdate(T entitie)
        //{
        //    List<T> entities = new List<T>();
        //    entities.Add(entitie);

        //    dbSet.AddOrUpdate(entities.ToArray());
        //}

        public virtual void AddRange(IEnumerable<T> entities)
        {
            dbSet.AddRange(entities);
        }

        public virtual void RemoveRange(IEnumerable<T> entities)
        {
            dbSet.RemoveRange(entities);
        }

        public virtual void Delete(object id)
        {
            T entityToDelete = dbSet.Find(id);
            Delete(entityToDelete);
        }

        public virtual void Delete(params object[] id)
        {
            T entityToDelete = dbSet.Find(id);
            Delete(entityToDelete);
        }

        public virtual void Delete(T entityToDelete)
        {
            if (db_Context.Entry(entityToDelete).State == EntityState.Detached) dbSet.Attach(entityToDelete);
            dbSet.Remove(entityToDelete);
        }

        public virtual void Update(T entityToUpdate)
        {
            dbSet.Attach(entityToUpdate);
            db_Context.Entry(entityToUpdate).State = EntityState.Modified;
        }

        public bool Exists()
        {
            IQueryable<T> query = dbSet;
            return query.Any();
        }

        public bool Exists(Expression<Func<T, bool>> expr)
        {
            IQueryable<T> query = dbSet;
            return query.Any(expr);
        }

        public IQueryable<T> Include(params Expression<Func<T, object>>[] includeExpressions)
        {
            //IDbSet<T> dbSet = Context.Set<T>();

            IQueryable<T> query = null;
            foreach (var includeExpression in includeExpressions)
            {
                query = dbSet.Include(includeExpression);
            }

            return query ?? dbSet;
        }



        //Async

        public virtual async Task<IEnumerable<T>> Get_async()
        {
            return await Get_async(null, null, null);
        }

        public virtual async Task<IEnumerable<T>> Get_async(Expression<Func<T, bool>> filter)
        {
            return await Get_async(filter, null, null);
        }

        public virtual async Task<IEnumerable<T>> Get_async(Func<IQueryable<T>, IOrderedQueryable<T>> orderBy)
        {
            return await Get_async(null, orderBy, null);
        }

        public virtual async Task<IEnumerable<T>> Get_async(Func<IQueryable<T>, IIncludableQueryable<T, object>> include)
        {
            return await Get_async(null, null, include);
        }

        public virtual async Task<IEnumerable<T>> Get_async(Expression<Func<T, bool>> filter, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy)
        {
            return await Get_async(filter, orderBy, null);
        }

        public virtual async Task<IEnumerable<T>> Get_async(Expression<Func<T, bool>> filter, Func<IQueryable<T>, IIncludableQueryable<T, object>> include)
        {
            return await Get_async(filter, null, include);
        }


        public virtual async Task<IEnumerable<T>> Get_async(Expression<Func<T, bool>> filter = null,
                                                            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
                                                            Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null,
                                                            bool NoTrancking = false)
        {
            IQueryable<T> query;

            if (NoTrancking == true) { query = dbSet.AsNoTracking(); } else { query = dbSet; }

            if (filter != null) query = query.Where(filter);

            if (include != null) query = include(query);

            if (orderBy != null) return await orderBy(query).ToListAsync(); else return await query.ToListAsync();
        }

        public virtual async Task<T> GetById_async(object id)
        {
            return await dbSet.FindAsync(id);
        }

        public virtual async Task<T> GetById_async(params object[] id)
        {
            return await dbSet.FindAsync(id);
        }

        public async Task<int> Count_async()
        {
            return await dbSet.CountAsync();
        }

    }
}
