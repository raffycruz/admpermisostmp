﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Project.SqlRepository.CustomModel
{
    //[ModelMetadataType(typeof(PermisoMetadata))]
    public class Permiso
    {
        [Key]
        public int Id { get; set; }
        public string NombreEmpleado { get; set; }
        public string ApellidosEmpleado { get; set; }
        [Column("TipoPermiso")]
        public int TipoPermisoId { get; set; }
        public DateTime FechaPermiso { get; set; }

        [ForeignKey("TipoPermisoId")]
        public TipoPermiso TipoPermiso { get; set; }
    }


    //[Table("Permiso", Schema = "dbo")]
    //public class PermisoMetadata
    //{
    //    [Key]
    //    public int Id { get; set; }

    //    [Column("TipoPermiso")]
    //    public int TipoPermisoId2 { get; set; }
    //}

}
