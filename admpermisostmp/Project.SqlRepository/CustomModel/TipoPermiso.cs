﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Project.SqlRepository.CustomModel
{
    //[MetadataType(typeof(TipoPermisoMetadata))]
    public class TipoPermiso
    {
        [Key]
        public int Id { get; set; }
        public string Descripcion { get; set; }

        // public ICollection<Permiso> Permisos { get; set; }
    }

    //[Table("TipoPermiso", Schema = "dbo")]
    //public class TipoPermisoMetadata
    //{
    //    [Key]
    //    public int Id { get; set; }
    //}

}
