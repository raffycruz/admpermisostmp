﻿//using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Project.Contract;
using Project.SqlRepository.CustomModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Project.SqlRepository.Services
{
    public class PermisoService : IPermisoService<Permiso>
    {
        private readonly IUnitOfWork db;

        public PermisoService(IUnitOfWork db)
        {
            this.db = db;
        }

        public async Task<IEnumerable<Permiso>> Get()
        {
            var model = await db.GetRepository<Permiso>().Get_async(source => source.Include(x=> x.TipoPermiso));
            return model;
        }

        public async Task<Permiso> GetById(int id)
        {
            var model = await db.GetRepository<Permiso>().GetById_async(id);
            return model;
        }

        public async Task<Permiso> Insert(Permiso Permiso)
        {
            db.GetRepository<Permiso>().Insert(Permiso);
            await db.Save_async();
            return Permiso;
        }

        public async Task<Permiso> Update(Permiso Permiso)
        {
            db.GetRepository<Permiso>().Update(Permiso);
            await db.Save_async();
            return Permiso;
        }

        public async Task<int> Delete(object id)
        {
            var Permiso = db.GetRepository<Permiso>().GetById(id);
            db.GetRepository<Permiso>().Delete(Permiso);
            return await db.Save_async();
        }

        public async Task<int> Delete(params object[] id)
        {
            db.GetRepository<Permiso>().Delete(id);
            return await db.Save_async();
        }

        public async Task<int> Delete(Permiso Permiso)
        {
            db.GetRepository<Permiso>().Delete(Permiso);
            return await db.Save_async();
        }

        public async Task<int> Count()
        {
            return await db.GetRepository<Permiso>().Count_async();
        }


    }
}
