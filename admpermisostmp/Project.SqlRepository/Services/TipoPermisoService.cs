﻿using Microsoft.EntityFrameworkCore;
using Project.Contract;
using Project.SqlRepository.CustomModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Project.SqlRepository.Services
{
    public class TipoPermisoService : ITipoPermisoService<TipoPermiso>
    {
        private readonly IUnitOfWork db;

        public TipoPermisoService(IUnitOfWork db)
        {
            this.db = db;
        }

        public async Task<IEnumerable<TipoPermiso>> Get()
        {
            var model = await db.GetRepository<TipoPermiso>().Get_async();
            return model;
        }

        public async Task<TipoPermiso> GetById(int id)
        {
            var model = await db.GetRepository<TipoPermiso>().GetById_async(id);
            return model;
        }

        public async Task<TipoPermiso> Insert(TipoPermiso TipoPermiso)
        {
            db.GetRepository<TipoPermiso>().Insert(TipoPermiso);
            await db.Save_async();
            return TipoPermiso;
        }

        public async Task<TipoPermiso> Update(TipoPermiso TipoPermiso)
        {
            db.GetRepository<TipoPermiso>().Update(TipoPermiso);
            await db.Save_async();
            return TipoPermiso;
        }

        public async Task<int> Delete(object id)
        {
            var TipoPermiso = db.GetRepository<TipoPermiso>().GetById(id);
            db.GetRepository<TipoPermiso>().Delete(TipoPermiso);
            return await db.Save_async();
        }

        public async Task<int> Delete(params object[] id)
        {
            db.GetRepository<TipoPermiso>().Delete(id);
            return await db.Save_async();
        }

        public async Task<int> Delete(TipoPermiso TipoPermiso)
        {
            db.GetRepository<TipoPermiso>().Delete(TipoPermiso);
            return await db.Save_async();
        }

        public async Task<int> Count()
        {
            return await db.GetRepository<TipoPermiso>().Count_async();
        }


    }
}
