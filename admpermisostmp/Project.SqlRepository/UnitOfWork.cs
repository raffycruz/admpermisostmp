﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Project.Contract;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Project.SqlRepository
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IDictionary<Type, object> _repositories = new Dictionary<Type, object>();
        private readonly IDB_Context db_Context;

        public UnitOfWork(IDB_admPermisostemp_Context context)
        {
            db_Context = context;
        }

        private bool _Disposed = false;

        public IBaseRepository<T> GetRepository<T>() where T : class
        {
            var type = typeof(T);

            if (!_repositories.ContainsKey(type))
            {
                var instance = (BaseRepository<T>)Activator.CreateInstance(typeof(BaseRepository<T>), db_Context);
                _repositories[type] = instance;
            }

            return _repositories[type] as BaseRepository<T>;
        }

        public void EntryDetached(object T)
        {
            db_Context.Entry(T).State = EntityState.Detached;
        }

        public void Save()
        {
            db_Context.SaveChanges();
        }

        public EntityEntry Entry(object entity)
        {
            return db_Context.Entry(entity);
        }

        public async virtual Task<int> Save_async()
        {
            return await db_Context.SaveChangesAsync();
        }

        public IDatabaseTransaction BeginTransaction()
        {
            return new EntityDatabaseTransaction(db_Context);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_Disposed) if (disposing) db_Context.Dispose();
            _Disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
