﻿using Microsoft.EntityFrameworkCore.Storage;
using Project.Contract;
using System;
using System.Collections.Generic;
using System.Text;

namespace Project.SqlRepository
{
    public class EntityDatabaseTransaction : IDatabaseTransaction
    {
        private IDbContextTransaction _transaction;

        public EntityDatabaseTransaction(IDB_Context context)
        {
            _transaction = context.Database.BeginTransaction();
        }

        public void Commit()
        {
            _transaction.Commit();
        }

        public void Rollback()
        {
            _transaction.Rollback();
        }

        public void Dispose()
        {
            _transaction.Dispose();
        }
    }
}
