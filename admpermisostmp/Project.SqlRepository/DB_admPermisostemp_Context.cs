﻿using Microsoft.EntityFrameworkCore;
using Project.Contract;
using System;
using System.Collections.Generic;
using System.Text;
using Project.SqlRepository.CustomModel;

namespace Project.SqlRepository
{
    public partial class DB_admPermisostemp_Context : DB_Context, IDB_admPermisostemp_Context
    {

        public DB_admPermisostemp_Context(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        //    modelBuilder.Entity<Permiso>()
        //        .HasOne(p => p.TipoPermiso)
        //.WithMany(b => b.Posts)
        //.HasForeignKey(p => p.BlogForeignKey);

            base.OnModelCreating(modelBuilder);
        }

        public virtual DbSet<Permiso> Permiso { get; set; }
        public virtual DbSet<TipoPermiso> TipoPermiso { get; set; }

    }
}
