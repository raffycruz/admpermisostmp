﻿using Project.Contract;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Project.SqlRepository.CustomModel;

namespace Project.SqlRepository
{
    public class DB_Context : DbContext, IDB_Context
    {
        public DB_Context(DbContextOptions options) : base(options)
        {
        }
    }
}
